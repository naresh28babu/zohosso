//$Id$
package com.zoho.oauth;

public class OAuthConstants {

	 static final String IAM_URL="IAM_URL";	
	
	 static final String SCOPE="scope";
	
	 static final String STATE = "state";
	 static final String STATE_OBTAINING_GRANT_TOKEN = "OBTAIN_GRANT_TOKEN";
	 static final String CLIENT_ID="client_id";	
	 static final String CLIENT_SECRET="client_secret";
	
	 static final String RESPONSE_TYPE="response_type";	
	 static final String RESPONSE_TYPE_CODE="code";	
	
	 static final String REDIRECT_URI="redirect_uri";	
	
	 static final String ACCESS_TYPE="access_type";	
	 static final String ACCESS_TYPE_OFFLINE="offline";	
	 static final String ACCESS_TYPE_ONLINE="online";	

	
	 static final String PROMPT="prompt";	
	 static final String PROMPT_CONSENT="consent";	
	
	
	 static final String GRANT_TYPE="grant_type";	
	 static final String GRANT_TYPE_AUTH_CODE="authorization_code";	
	 static final String GRANT_TYPE_REFRESH="refresh_token";

	 static final String IAMURL = "IAMUrl";

	 static final String CODE = "code";	
	 static final String ACCESS_TOKEN = "access_token";	
	 static final String REFRESH_TOKEN = "refresh_token";	
	 static final String EXPIRES_IN = "expires_in";	

	 static final String PERSISTENCE_IMPL = "Persistence_hanlder";

	 static final String TOKEN = "token";	
	
	 static final String OAUTH_PARAMS = "OAUTH_PARAMS";
	 static final String DISPATCH_TO = "dispatchTo";

}
